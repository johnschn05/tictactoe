
#
# ───────────────────────────────────────────────────────── GLOBAL VARIABLES ─────
#
# ─── GAME BOARD ─────────────────────────────────────────────────────────────────

board = ["-", "-", "-",
         "-", "-", "-",
         "-", "-", "-"]

# ─── IS GAME STILL GOING? ───────────────────────────────────────────────────────

game_still_going = True

# ─── WHO WON? OR TIE? ───────────────────────────────────────────────────────────

winner = None

# ─── WHOS TURN IS IT ────────────────────────────────────────────────────────────

current_player = "X"

#
# ───────────────────────────────────────────────────── FUNCTION DEFINITIONS ─────
#
# ─── GAMEPLAY ──────────────────────────────────────────────────────────────────

def play_game():
    display_board()                     # Display initial board

    while game_still_going:             # While game_still_going = True, perform loop
        handle_turn(current_player)     # handle a single turn of an arbitrary player
        check_if_game_over()            # check if the game has ended
        flip_player()                   # Flip to the other player

    if winner == "X" or winner == "O":  # The game has ended and there is a winner...
        print(winner + " won.")         # Print winner "X" or "O"
    elif winner == None:                # If there is no winner
        print("Tie.")                   # Print "Tie"

# ─── DISPLAY GAME BOARD ─────────────────────────────────────────────────────────

def display_board():
    print("\n")
    print(board[0] + " | " + board[1] + " | " + board[2] + "     1 | 2 | 3")
    print(board[3] + " | " + board[4] + " | " + board[5] + "     4 | 5 | 6")
    print(board[6] + " | " + board[7] + " | " + board[8] + "     7 | 8 | 9")
    print("\n")

# ─── HANDLING A PLAYERS TURN ────────────────────────────────────────────────────

def handle_turn(player):
    print(player + "'s turn.")
    position = input("Choose a position from 1-9: ")
    valid = False

    while not valid:
        while position not in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            position = input("Choose a position from 1-9: ")
        position = int(position) - 1
        if board[position] == "-":
            valid = True
        else:
            print("You cant go there. Go again.")

    board[position] = player
    display_board()

# ─── VERIFY IF GAME IS OVER ─────────────────────────────────────────────────────

def check_if_game_over():
    check_for_winner()
    check_if_tie()

# ─── VERIFY IF THERE IS A WINNER ────────────────────────────────────────────────

def check_for_winner():
    global winner                           # import global variable "winner"
    row_winner = check_rows()               # check rows for a winner
    column_winner = check_columns()         # check columns for a winner
    diagonal_winner = check_diagonals()     # check diagonals for a winner

    if row_winner:
        winner = row_winner
    elif column_winner:
        winner = column_winner
    elif diagonal_winner:
        winner = diagonal_winner
    else:
        winner = None
    return

# ─── CHECK ROWS FOR A WINNER ────────────────────────────────────────────────────

def check_rows():
    global game_still_going                             # import global variable "game_still_going"

    row_1 = board[0] == board[1] == board[2] != "-"     # check if any of the rows have the same value (and is not empty)
    row_2 = board[3] == board[4] == board[5] != "-"
    row_3 = board[6] == board[7] == board[8] != "-"

    if row_1 or row_2 or row_3:                         # if rows match, flag that there is a win
        game_still_going = False
    if row_1:                                           # return winner (X or O) for row 1, 2 or 3
        return board[0]
    if row_2:
        return board[3]
    if row_3:
        return board[6]
    return

# ─── CHECK COLUMNS FOR A WINNER ─────────────────────────────────────────────────

def check_columns():
    global game_still_going                                 # import global variable "game_still_going"

    column_1 = board[0] == board[3] == board[6] != "-"      # check if any of the columns have the same value (and is not empty)
    column_2 = board[1] == board[4] == board[7] != "-"
    column_3 = board[2] == board[5] == board[8] != "-"

    if column_1 or column_2 or column_3:                    # if columns match, flag that there is a win
        game_still_going = False
    if column_1:                                            # return winner (X or O) for column 1, 2 or 3
        return board[0]
    if column_2:
        return board[1]
    if column_3:
        return board[2]
    return

# ─── CHECK DIAGONALS FOR A WINNER ───────────────────────────────────────────────

def check_diagonals():
    global game_still_going                                 # import global variable "game_still_going"

    diagonal_1 = board[0] == board[4] == board[8] != "-"    # check if any of the diagonals have the same value (and is not empty)
    diagonal_2 = board[6] == board[4] == board[2] != "-"

    if diagonal_1 or diagonal_2:                            # if diagonals match, flag that there is a win
        game_still_going = False
    if diagonal_1:                                          # return winner (X or O) for diagonal 1 or 2
        return board[0]
    if diagonal_2:
        return board[6]
    return

# ─── CHECK FOR A TIE GAME ───────────────────────────────────────────────────────

def check_if_tie():
    global game_still_going             # import global variable "game_still_going"
    if "-" not in board:                # if all positions are filled and winner = none
        game_still_going = False        # game_still_going value is changed to "False"
    return

# ─── FLIPPING PLAYERS ────────────────────────────────────────────────────────────

def flip_player():
    global current_player               # import global variable "current_player"
    if current_player == "X":           # If current_player is "X" then change to "O"
        current_player = "O"
    elif current_player == "O":         # if current_player is "O" then change to "X"
        current_player = "X"
    return

#
# ───────────────────────────────────────────────────────── START OF PROGRAM ─────
#

play_game()



#
# ──────────────────────────────────────── OUTLINE WHAT WE NEED FOR OUR GAME ─────
#
# board
# display board
# play game
# handle turn
# check win
    # check rows
    # check columns
    #check diagonals
# check tie
# flip player


